def count_men(filename):
    with open(filename) as f:
        lines = f.readlines()

    men_count = 0
    for line in lines:
        men_count += line.count(' men.')
        men_count += line.count(' men ')
        men_count += line.count(' men,')
        men_count += line.count('Men ')
        men_count += line.count('Men,')

    return men_count


def capitalize_women(filename):
    with open(filename) as f:
        lines = f.readlines()

    newlines = []
    for line in lines:
        newline = line.replace('women', 'WOMEN')
        newlines.append(newline)

    with open('example_text_new.txt', 'w') as f:
        for newline in newlines:
            f.write(newline)


def contains_blue_devil(filename):
    with open(filename) as f:
        lines = f.readlines()

    for line in lines:
        if 'Blue Devil' in line:
            return True

    return False


def find_non_terminal_said(filename):
    import re
    with open(filename) as f:
        lines = f.readlines()

    saidregex = re.compile('said(?!\.)')
    non_term_inst = 0
    for line in lines:
        for x in saidregex.finditer(line):
            non_term_inst += 1

    return non_term_inst
